import React from "react";
import styles from "./P.module.css";

export default function P({ title }) {
  return <p className={styles.paragraph}>{title}</p>;
}
