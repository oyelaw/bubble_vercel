import React from "react";
import styled from "styled-components";
import { useTable } from "react-table";

const makeData = [
  {
    "#": "0001",
    orderDetails: "9 Items bought",
    date: "24 February, 2021",
    pointValue: "+940",
    nairaPointValue: "₦9,800",
    viewMore: "View Order",
  },
  {
    "#": "0002",
    orderDetails: "4 Items bought",
    date: "24 February, 2021",
    pointValue: "+440",
    nairaPointValue: "₦4,800",
    viewMore: "View Order",
  },
  {
    "#": "0003",
    orderDetails: "2 Items bought",
    date: "24 February, 2021",
    pointValue: "+240",
    nairaPointValue: "₦2,800",
    viewMore: "View Order",
  },
  {
    "#": "0004",
    orderDetails: "7 Items bought",
    date: "24 February, 2021",
    pointValue: "+740",
    nairaPointValue: "₦7,800",
    viewMore: "View Order",
  },
];
const Styles = styled.div`
  width: 1000;
  padding: 1rem;

  .more {
    color: #0c8dba;
  }

  .points {
    color: #00a137;
  }

  table {
    width: 100%;
    border-spacing: 0;
    border: 1px solid #dee2e6;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid #dee2e6;
      border-right: 1px solid #dee2e6;

      :last-child {
        border-right: 0;
      }
    }
  }
`;

function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
  });

  // Render the UI for your table
  return (
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps()}>{column.render("Header")}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => {
                return (
                  <td
                    {...cell.getCellProps([
                      {
                        className: cell.column.className,
                        style: cell.column.style,
                      },
                    ])}
                  >
                    {cell.render("Cell")}
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

function App() {
  const columns = React.useMemo(
    () => [
      {
        Header: "#",
        accessor: "#", // accessor is the "key" in the data
      },
      {
        Header: "Order Details",
        accessor: "orderDetails",
      },
      {
        Header: "Date",
        accessor: "date",
      },
      {
        Header: "Point Value",
        accessor: "pointValue",
        className: "points",
      },
      {
        Header: "Naira Point Value",
        accessor: "nairaPointValue",
      },
      {
        Header: "",
        accessor: "viewMore",
        className: "more",
        style: {
          fontWeight: "bolder",
          cursor: "pointer",
        },
      },
    ],
    []
  );

  const data = React.useMemo(() => makeData, []);

  return (
    <Styles>
      <Table columns={columns} data={data} />
    </Styles>
  );
}

export default App;
