import React from "react";
import Typography from "../Typography/Typography";
import styles from "./NewsLetter.module.css";

export default function NewsLetter() {
  return (
    <div className={styles.newsLetterSection}>
      <Typography
        variant="h2"
        color="black"
        style={{ marginBottom: "1em", marginTop: "2.5em" }}
      >
        New to Bubble Colony?
      </Typography>

      <Typography
        variant="body3"
        color="black"
        style={{ marginBottom: "1em", maxWidth: "598px" }}
      >
        Subscribe to our newsletter to stay updated with the latest news and
        special sales. Leave your email address here!
      </Typography>

      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <input
          type="text"
          placeholder="Enter your email address"
          name="search"
          className={styles.emailInput}
        />
        <button className={styles.subscribeButton}>Subscribe</button>
      </div>
    </div>
  );
}
