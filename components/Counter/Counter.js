import React from "react";

import styles from "./Counter.module.css";

import Typography from "../Typography/Typography";

export default function Counter({ count }) {
  const [state, setState] = React.useState(0);

  const handleDecrease = () => {
    if (state == 0) {
      return 0;
    }
    return setState((state) => state - 1);
  };

  const handleIncrease = () => {
    return setState((state) => state + 1);
  };
  return (
    <div className={styles.counter}>
      <button className={styles.counterMinus} onClick={handleDecrease}>
        -
      </button>
      <Typography variant="body4" color="black" style={{ margin: "0 1em" }}>
        {state}
      </Typography>
      <button className={styles.counterPlus} onClick={handleIncrease}>
        +
      </button>
    </div>
  );
}
