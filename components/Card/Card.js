import React from "react";
import Link from "next/link";
import Image from "next/image";
import ReactStars from "react-rating-stars-component";

import Typography from "../Typography/Typography";
import Button from "../Button/Button";

import styles from "./Card.module.css";

export default function Card({ title, body, styleProp, to = "/", style }) {
  return (
    <Link href={to}>
      <div className={styles.Box}>
        <Typography
          children={title}
          variant="h3"
          color="black"
          style={{ marginBottom: "0.8125em", padding: "1em 0 0 1em" }}
        />

        <div className={styleProp} style={style}>
          {body}
        </div>
      </div>
    </Link>
  );
}

export function CardWithButton({ title, body, styleProp, to = "/", style }) {
  return (
    <Link href={to}>
      <div className={styles.Box}>
        <Typography
          children={title}
          variant="h3"
          color="black"
          style={{ marginBottom: "0.8125em", padding: "1em 0 0 1em" }}
        />
        <div style={{ width: "100%", padding: "1em" }}>
          <Button
            text={body}
            style={{ width: "100%", color: "white", height: "48px" }}
          />
        </div>
      </div>
    </Link>
  );
}

const boxStyle = {
  backgroundColor: "#F1F4F4",
};

const headerStyle = { padding: ".5em 0", textAlign: "center" };

const imageStyle = { width: "100%", height: "calc(100% - 2em)" };
export function CardWithGrid({ title, body, styleProp, to = "/", style }) {
  return (
    <Link href={to}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          backgroundColor: "white",
        }}
      >
        <Typography
          children={title}
          variant="h3"
          color="black"
          style={{ padding: "1em 0 0 1em" }}
        />

        <div
          style={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            gridGap: "1em",
            padding: "1em",
            height: "100%",
            maxHeight: "100%",
          }}
        >
          <div style={boxStyle}>
            <img src="/assets/clothes.png" alt="me" style={imageStyle} />
            <Typography variant="h5" color="black" style={headerStyle}>
              Clothing
            </Typography>
          </div>

          <div style={boxStyle}>
            <img src="/assets/funiture.png" alt="me" style={imageStyle} />
            <Typography variant="h5" color="black" style={headerStyle}>
              Toys
            </Typography>
          </div>

          <div style={boxStyle}>
            <img src="/assets/jungleBook.png" alt="me" style={imageStyle} />
            <Typography variant="h5" color="black" style={headerStyle}>
              Books
            </Typography>
          </div>

          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              backgroundColor: "#F1F4F4",
            }}
          >
            <Typography variant="h5" color="black" style={headerStyle}>
              All Categories
            </Typography>
          </div>
        </div>
      </div>
    </Link>
  );
}

export function CardImage({ title, body, styleProp, to = "/", style }) {
  return (
    <Link href={to}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
          backgroundColor: "white",
        }}
      >
        <Typography
          children={title}
          variant="h3"
          color="black"
          style={{ padding: "1em 0 0 1em" }}
        />

        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            paddingBottom: "0.45em",
            borderRadius: "4px",
            padding: "1em",
          }}
        >
          {body}
        </div>
      </div>
    </Link>
  );
}

export function QuestionAndAnswerCard({ vote, header, answer, postedBy }) {
  return (
    <div className={styles.question}>
      <div className={styles.vote}>
        <Image src="/assets/upvote.png" alt="me" width="19" height="13" />

        <Typography
          children={vote}
          variant="body3"
          style={{ margin: ".2em, 0" }}
        />
        <Typography
          children="Vote"
          variant="body4"
          style={{ margin: ".2em, 0" }}
        />
        <Image src="/assets/downvote.png" alt="me" width="19" height="13" />
      </div>
      <div className={styles.answer}>
        <Typography
          children={header}
          variant="h4"
          style={{ marginBottom: "1em" }}
        />

        <Typography
          children={answer}
          variant="body3"
          style={{ marginBottom: "1em", maxWidth: "330px" }}
        />

        <Typography
          children={postedBy}
          variant="body4"
          style={{ maxWidth: "330px", marginTop: "1em" }}
          color="grey"
        />
      </div>
    </div>
  );
}

export function CustomerReviewCard() {
  return (
    <div className={styles.review}>
      <Typography children="My favorite shirt" variant="h3" color="black" />
      <ReactStars count={5} value={3} size={10} activeColor="#ffd700" />
      <Typography
        children="By Austine on February 25, 2021"
        variant="body4"
        color="grey"
        style={{ marginTop: "0" }}
      />
      <Typography
        children="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tincidunt
      malesuada montes, tortor tellus faucibus senectus ultrices."
        variant="body4"
        color="grey"
        style={{ margin: "1em 0" }}
      />
      <div style={{ display: "flex", justifyItems: "center" }}>
        <Typography
          children="66 people found this helpful. Was this helpful to you?"
          variant="body4"
          color="grey"
          // style={{ marginRight: ".1em" }}
        />
        <div style={{ display: "flex" }}>
          <Button
            text="Yes"
            style={{
              backgroundColor: "#f0f1f1",
              width: "39px",
              height: "26px",
              marginRight: "1em",
              color: "black",
            }}
          />
          <Button
            text="No"
            style={{
              backgroundColor: "#f0f1f1",
              width: "39px",
              height: "26px",
              marginLeft: "1em",
              color: "black",
            }}
          />
        </div>
      </div>
    </div>
  );
}
