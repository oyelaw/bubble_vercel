import React from "react";

import styles from "./Header.module.css";

export default function Header({ title }) {
  return <h5 className={styles.title}>{title}</h5>;
}
