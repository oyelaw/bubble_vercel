import React from "react";
import Image from "next/image";

import styles from "./ImageWithOverlay.module.css";
import Typography from "../Typography/Typography";

export default function ImageWithOverlay({ text, position, image }) {
  return (
    <div className={styles.container}>
      <img src={image} alt="Snow" width="100%" height="100%" />
      <Typography
        variant="h3"
        color="white"
        className={styles.centered}
        children={text}
      />
    </div>
  );
}
