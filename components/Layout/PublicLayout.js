import React from "react";
import { HeaderWithoutSignin, HeaderWithSearchBar } from "./Header";
import Footer from "./Footer";

import styles from "./Layout.module.css";

export default function Layout({ children }) {
  return (
    <div className={styles.layout} style={{ backgroundColor: "#E5E5E5" }}>
      <HeaderWithoutSignin login />
      <div className={styles.children}>{children}</div>

      <Footer />
    </div>
  );
}

export function RegisterLayout({ children }) {
  return (
    <div className={styles.layout} style={{ backgroundColor: "#E5E5E5" }}>
      <HeaderWithoutSignin register />
      <div className={styles.children}>{children}</div>

      <Footer />
    </div>
  );
}

export function LayoutWithSearchBarInHeader({ children }) {
  return (
    <div className={styles.layout}>
      <HeaderWithSearchBar />
      <div className={styles.children}>{children}</div>

      <Footer />
    </div>
  );
}
