import React from "react";
import Image from "next/image";

import Typography from "../components/Typography/Typography";
import AccountSidebar from "../components/Partials/AccountSidebar";
import Link from "next/link";

const containerStyle = {
  backgroundColor: "#E5E5E5",
  display: "grid",
  gridTemplateColumns: "1fr 4fr",
  gridGap: "1em",
  padding: "1.7em",
  flex: 1,
};

const leftPane = {
  backgroundColor: "#fff",
  maxHeight: "310px",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const rightPane = {
  backgroundColor: "#fff",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const iconsText = {
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
};

const active = {
  backgroundColor: "#F1F4F4",
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  padding: ".4em",
};

const buttonStyle = {
  width: "100%",
  backgroundColor: "#fff",
  color: "#0c8dba",
  alignSelf: "center",
  borderTop: "1px solid #E0E0E0",
  padding: "1em",
  marginTop: ".5em",
};

const boxStyle = {
  height: "192px",
  background: "rgba(12, 141, 186, 0.03)",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
};

const EditButtonStyle = {
  width: "100%",
  backgroundColor: "inherit",
  color: "#0c8dba",
  textAlign: "left",
  marginTop: ".5em",
};

const profileRow = {
  display: "flex",
  justifyItems: "space-between",
};
export default function Account() {
  const [showTable, setTable] = React.useState(false);
  return (
    <div style={containerStyle}>
      <AccountSidebar activeStyleIndex={1} />
      <div>
        <div style={{ backgroundColor: "#E5E5E5", width: "100%" }}>
          <Typography
            children="Go Back"
            variant="body4"
            color="blue"
            style={{ marginBottom: "1em" }}
          />
        </div>
        <div style={rightPane}>
          <Typography
            children="Address Book"
            variant="h3"
            color="black"
            style={{ marginBottom: "1em" }}
          />
          <div
            style={{
              display: "flex",
              borderTop: "1px solid #E0E0E0",
              paddingTop: "1em",
              flexDirection: "column",
              minHeight: "500px",
            }}
          >
            <div>
              <div
                tyle={{
                  backgroundColor: "#fff",
                  display: "flex",
                  flexDirection: "columen",
                  justifyItems: "center",
                  padding: "1em",
                }}
              >
                <div
                  style={{
                    display: "grid",
                    gridTemplateColumns: "1fr 1fr",
                    gridGap: "1em",
                    justifyContent: "space-around",
                    position: "relative",
                  }}
                >
                  <Link href="/addressBookNewAddress">
                    <div
                      style={{
                        backgroundColor: "rgba(12, 141, 186, 0.05)",
                        borderRadius: "4px",
                        display: "flex",
                        flexDirection: "column",
                        padding: "1em",
                        width: "100%",
                        justifyItems: "center",
                        alignItems: "center",
                        border: "1px dashed #e0e0e0",
                        marginRight: "1em",
                      }}
                    >
                      <Image
                        src="/assets/location1.png"
                        alt="me"
                        width="108"
                        height="108"
                      />
                      <Typography
                        children="Add New Address"
                        variant="h3"
                        color="black"
                        style={{ marginBottom: "1em" }}
                      />
                    </div>
                  </Link>

                  <div
                    style={{
                      backgroundColor: "rgba(12, 141, 186, 0.05)",
                      borderRadius: "4px",
                      display: "flex",
                      flexDirection: "column",
                      padding: "1em",
                      width: "100%",
                      border: "1px dashed #e0e0e0",
                    }}
                  >
                    <div
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                        backgroundColor: "#0c8dba",
                        padding: "0.1em",
                        borderRadius: "2px",
                      }}
                    >
                      <Image
                        src="/assets/check.png"
                        alt="me"
                        width="9"
                        height="9"
                      />
                    </div>
                    <Typography
                      variant="h3"
                      color="black"
                      style={{ marginBottom: "0.625em" }}
                    >
                      Ifeanyi Umunnakwe
                    </Typography>
                    <Typography
                      variant="body2"
                      color="black"
                      style={{ marginBottom: "1em" }}
                    >
                      Quits Aviation Services Free Zone Murtala Muhammed
                      International Airport 23401, LagosQuits Aviation Services
                      Free Zone Murtala Muhammed International Airport 23401,
                      Lagos
                    </Typography>
                    <Typography
                      variant="h5"
                      color="black"
                      style={{ marginBottom: "0.875em" }}
                    >
                      Phone number:
                    </Typography>
                    <Typography
                      variant="body3"
                      color="black"
                      style={{ marginBottom: "0.875em" }}
                    >
                      080454395930
                    </Typography>

                    <div
                      style={{
                        marginTop: "1em",
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <Typography
                        children="Remove"
                        variant="h6"
                        style={{ color: "#ba390c", marginRight: "1em" }}
                      />{" "}
                      |
                      <Typography
                        children="Edit"
                        variant="h6"
                        style={{
                          color: "#0c8dba",
                          marginLeft: "1em",
                          marginRight: "1em",
                        }}
                      />
                    </div>
                  </div>
                  <div
                    style={{
                      backgroundColor: "rgba(12, 141, 186, 0.05)",
                      borderRadius: "4px",
                      display: "flex",
                      flexDirection: "column",
                      padding: "1em",
                      width: "100%",
                      border: "1px dashed #e0e0e0",
                    }}
                  >
                    <div
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                        backgroundColor: "#0c8dba",
                        padding: "0.1em",
                        borderRadius: "2px",
                      }}
                    >
                      <Image
                        src="/assets/check.png"
                        alt="me"
                        width="9"
                        height="9"
                      />
                    </div>
                    <Typography
                      variant="h3"
                      color="black"
                      style={{ marginBottom: "0.625em" }}
                    >
                      Ifeanyi Umunnakwe
                    </Typography>
                    <Typography
                      variant="body2"
                      color="black"
                      style={{ marginBottom: "1em" }}
                    >
                      Quits Aviation Services Free Zone Murtala Muhammed
                      International Airport 23401, LagosQuits Aviation Services
                      Free Zone Murtala Muhammed International Airport 23401,
                      Lagos
                    </Typography>
                    <Typography
                      variant="h5"
                      color="black"
                      style={{ marginBottom: "0.875em" }}
                    >
                      Phone number:
                    </Typography>
                    <Typography
                      variant="body3"
                      color="black"
                      style={{ marginBottom: "0.875em" }}
                    >
                      080454395930
                    </Typography>

                    <div
                      style={{
                        marginTop: "1em",
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <Typography
                        children="Remove"
                        variant="h6"
                        style={{ color: "#ba390c", marginRight: "1em" }}
                      />{" "}
                      |
                      <Typography
                        children="Edit"
                        variant="h6"
                        style={{
                          color: "#0c8dba",
                          marginLeft: "1em",
                          marginRight: "1em",
                        }}
                      />
                      |
                      <Typography
                        children="Set as Default"
                        variant="h6"
                        style={{ color: "#0c8dba", marginLeft: "1em" }}
                      />
                    </div>
                  </div>
                  <div
                    style={{
                      backgroundColor: "rgba(12, 141, 186, 0.05)",
                      borderRadius: "4px",
                      display: "flex",
                      flexDirection: "column",
                      padding: "1em",
                      width: "100%",
                      border: "1px dashed #e0e0e0",
                    }}
                  >
                    <div
                      style={{
                        position: "absolute",
                        top: "0",
                        right: "0",
                        backgroundColor: "#0c8dba",
                        padding: "0.1em",
                        borderRadius: "2px",
                      }}
                    >
                      <Image
                        src="/assets/check.png"
                        alt="me"
                        width="9"
                        height="9"
                      />
                    </div>
                    <Typography
                      variant="h3"
                      color="black"
                      style={{ marginBottom: "0.625em" }}
                    >
                      Ifeanyi Umunnakwe
                    </Typography>
                    <Typography
                      variant="body2"
                      color="black"
                      style={{ marginBottom: "1em" }}
                    >
                      Quits Aviation Services Free Zone Murtala Muhammed
                      International Airport 23401, LagosQuits Aviation Services
                      Free Zone Murtala Muhammed International Airport 23401,
                      Lagos
                    </Typography>
                    <Typography
                      variant="h5"
                      color="black"
                      style={{ marginBottom: "0.875em" }}
                    >
                      Phone number:
                    </Typography>
                    <Typography
                      variant="body3"
                      color="black"
                      style={{ marginBottom: "0.875em" }}
                    >
                      080454395930
                    </Typography>

                    <div
                      style={{
                        marginTop: "1em",
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <Typography
                        children="Remove"
                        variant="h6"
                        style={{ color: "#ba390c", marginRight: "1em" }}
                      />{" "}
                      |
                      <Typography
                        children="Edit"
                        variant="h6"
                        style={{
                          color: "#0c8dba",
                          marginLeft: "1em",
                          marginRight: "1em",
                        }}
                      />
                      |
                      <Typography
                        children="Set as Default"
                        variant="h6"
                        style={{ color: "#0c8dba", marginLeft: "1em" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
