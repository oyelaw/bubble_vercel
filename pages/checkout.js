import React from "react";
import Image from "next/image";
import Link from "next/link";
import { Formik } from "formik";
import * as Yup from "yup";

import styles from "../styles/Checkout.module.css";
import { LayoutWithSearchBarInHeader } from "../components/Layout/PublicLayout";
import Typography from "../components/Typography/Typography";
import Button from "../components/Button/Button";
import { TextInput, TextArea } from "../components/SelectInput/SelectInput";

const cx = (...classNames) => classNames.join(" ");

const schema = Yup.object().shape({
  address: Yup.string().required("Please type your address").trim(),
  city: Yup.string().required("Please type your city").trim(),
  state: Yup.string().required("Please select your state").trim(),
  fullname: Yup.string().required("Please type your fullname").trim(),
  phoneNumber: Yup.string().required("Please type your phone number").trim(),
});

function Checkout() {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <Typography children="Checkout" variant="h3" color="black" />
        <div style={{ display: "flex", alignItems: "center" }}>
          <div className={styles.circle}>
            <Image src="/assets/step1.png" width="16" height="16" />
          </div>
          <div className={styles.text}>Delivery Address</div>
          <div className={styles.dash}>
            <Image src="/assets/greyDash.png" width="16" height="3.48" />
          </div>
          <div className={styles.circle}>
            {" "}
            <Image src="/assets/step2.png" width="16" height="16" />
          </div>
          <div className={styles.text}>Payment Method</div>
        </div>
      </div>

      <div className={styles.addressAndSummary}>
        <div style={{ flex: "4" }}>
          <div
            style={{
              backgroundColor: "#fff",
              padding: "1em",
              marginBottom: "5em",
            }}
          >
            <Typography
              variant="h3"
              color="black"
              style={{ marginBottom: "1em" }}
            >
              Address
            </Typography>
            <div className={styles.formContainer}>
              <Typography
                variant="body3"
                color="black"
                style={{ paddingTop: "1.2em" }}
              >
                Please enter a shipping address for this order. Please also
                indicate whether your billing address is the same as the
                shipping address entered. When finished, click the "Continue"
                button.
              </Typography>

              <div>
                <Formik
                  initialValues={{
                    address: "",
                    city: "",
                    state: "",
                    fullname: "",
                    phoneNumber: "",
                  }}
                  validationSchema={schema}
                  enableReinitialize={true}
                  onSubmit={(values) => {
                    console.log(values);
                  }}
                >
                  {(props) => {
                    const {
                      handleChange,
                      values,
                      handleSubmit,
                      errors,
                      touched,
                    } = props;

                    return (
                      <>
                        <div className={styles.form}>
                          <TextInput
                            placeholder="Fullname"
                            onChangeText={handleChange("fullname")}
                            value={values.fullname}
                            className={styles.firstname}
                          />
                          <TextInput
                            placeholder="Phone number"
                            onChangeText={handleChange("phoneNumber")}
                            value={values.phoneNumber}
                            style={{ marginLeft: ".5em" }}
                            className={styles.lastname}
                          />

                          <TextArea
                            placeholder="Address"
                            onValueChange={handleChange("address")}
                            value={values.address}
                            className={styles.address}
                            row={5}
                            style={{ width: "100%" }}
                          />
                          <TextInput
                            placeholder="City"
                            onChangeText={handleChange("city")}
                            value={values.city}
                            className={styles.city}
                          />
                          <TextInput
                            placeholder="State"
                            onChangeText={handleChange("state")}
                            value={values.state}
                            // style={{ marginLeft: ".5em" }}
                            className={styles.state}
                          />
                        </div>
                        <Link href="/newAddress">
                          <Button
                            text="Continue to Payment"
                            style={{
                              padding: "1em 2em",
                              marginTop: "2em",

                              width: "100%",
                              height: "48px",

                              background: "#0c8dba",
                              border: "none",
                              borderRadius: "4px",
                              color: "#fff",
                            }}
                          />
                        </Link>
                      </>
                    );
                  }}
                </Formik>
              </div>
            </div>
          </div>
        </div>
        <div style={{ flex: "1", marginLeft: "1em" }}>
          <div className={styles.rightCard}>
            <Typography
              children="Order Summary"
              variant="h3"
              color="black"
              style={{ marginBottom: "1em" }}
            />
            <div className={styles.orderDetail}>
              <Typography variant="body2" children="Items(2):" />
              <Typography variant="body2" children="N29,500.00" />
            </div>
            <div className={styles.orderDetail}>
              <Typography variant="body2" children="Order Discounts:" />
              <Typography
                variant="body2"
                children="N29,500.00"
                style={{ color: "red" }}
              />
            </div>

            <div className={styles.orderDetail}>
              <Typography
                variant="body2"
                children=" Estimated Shipping:"
                style={{ margin: "2.31em 0" }}
              />

              <Typography
                variant="body2"
                children="  Add your Delivery address at checkout to see delivery charges"
                style={{ margin: "2.31em 0", maxWidth: "132px" }}
              />
            </div>

            <div className={styles.orderDetail}>
              <Typography variant="body2" children="Estimated Total:" />
              <Typography variant="body2" children="N28, 000.00" />
            </div>

            <div className={styles.icons}>
              <span className={styles.accept}>we accept</span>
              <Image src="/assets/mastercard.png" width="21" height="15" />
              <Image src="/assets/visa.png" width="47" height="15" />
              <Image src="/assets/verve.png" width="38" height="15" />
              <Image src="/assets/paga.png" width="53" height="15" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

Checkout.getLayout = (page) => (
  <LayoutWithSearchBarInHeader>{page}</LayoutWithSearchBarInHeader>
);

export default Checkout;
