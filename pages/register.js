import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";

import Typography from "../components/Typography/Typography";
import SelectInput, {
  TextInput,
  SwitchComponent,
} from "../components/SelectInput/SelectInput";
import { RegisterLayout } from "../components/Layout/PublicLayout";
import Button from "../components/Button/Button";

const schema = Yup.object().shape({
  email: Yup.string().required("Please type your email").trim(),
  password: Yup.string().required("Please type your password").trim(),
  fullname: Yup.string().required("Please type your fullname").trim(),
  phoneNumber: Yup.string().required("Please type your phone number").trim(),
});

function Register() {
  const [addChild, setAddChild] = React.useState(false);

  const handleClick = () => setAddChild((addChild) => !addChild);
  return (
    <div
      style={{
        backgroundColor: "#E5E5E5",
        display: "flex",

        justifyContent: "center",
        alignContent: "center",
        padding: "4rem",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Typography variant="h2" style={{ marginBottom: "2em" }}>
        Create Account
      </Typography>
      <div
        style={{
          backgroundColor: "white",
          justifyContent: "center",
          alignContent: "center",
          display: "flex",
          flexDirection: "column",
          padding: "1em",
        }}
      >
        <Formik
          initialValues={{
            email: "",
            password: "",
            fullname: "",
            phoneNumber: "",
          }}
          validationSchema={schema}
          enableReinitialize={true}
          onSubmit={(values) => {
            console.log(values);
          }}
        >
          {(props) => {
            const {
              handleChange,
              values,
              handleSubmit,
              errors,
              touched,
            } = props;

            return (
              <div
                style={{
                  backgroundColor: "#fff",
                  display: "flex",
                  flexDirection: "column",
                  justifyItems: "center",
                  padding: "1em",
                }}
              >
                <TextInput
                  placeholder="fullname"
                  onChange={handleChange("fullname")}
                  value={values.fullname}
                  style={{ width: "100%" }}
                  label="Your full name"
                />
                <TextInput
                  placeholder="email"
                  onChange={handleChange("email")}
                  value={values.email}
                  style={{ width: "100%" }}
                  label="Email Address"
                />
                <TextInput
                  placeholder="Phone number"
                  onChange={handleChange("phoneNumber")}
                  value={values.phoneNumber}
                  style={{ width: "100%" }}
                  label="Phone Number"
                />
                <TextInput
                  placeholder="Password"
                  onChange={handleChange("password")}
                  value={values.password}
                  style={{ width: "100%" }}
                  label="Password"
                />

                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Typography
                    variant="body3"
                    style={{
                      marginBottom: ".2em",
                      textAlign: "left",
                      maxWidth: "200px",
                    }}
                    color="blue"
                  >
                    Do you want to add your child’s profile?
                  </Typography>
                  <SwitchComponent onClick={handleClick} />
                </div>
                {addChild && (
                  <>
                    <Typography
                      variant="body3"
                      style={{ margin: ".5em", textAlign: "right" }}
                      color="error"
                    >
                      Remove
                    </Typography>
                    <TextInput
                      placeholder="Enter your child's name"
                      onChange={handleChange("password")}
                      value={values.password}
                      style={{ width: "100%" }}
                      label="Your childs name"
                    />
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <SelectInput
                        placeholder="Gender"
                        onChange={handleChange("password")}
                        value={values.password}
                        options={[
                          { value: "Male", label: "Male" },
                          { value: "Female", label: "Female" },
                        ]}
                        style={{
                          backgroundColor: " rgba(12, 141, 186, 0.02)",
                          border: "1px solid #E0E0E0",
                          height: "48px",
                          marginBottom: "0",
                        }}
                        label="Gender"
                      />
                      <TextInput
                        placeholder="Age"
                        onChange={handleChange("password")}
                        value={values.password}
                        style={{
                          marginLeft: ".5em",
                          marginBottom: "0",
                          marginTop: "0",
                        }}
                        label="Age"
                      />
                    </div>
                    <Button
                      text="Add another child’s profile"
                      style={{
                        padding: "1em 2em",
                        marginTop: "2em",

                        width: "100%",

                        background: "#fff",
                        border: "none",
                        borderRadius: "4px",
                        color: "#0c8dba",
                      }}
                    />
                  </>
                )}
                <Button
                  text="Create Account"
                  style={{
                    padding: "1em 2em",
                    marginTop: "2em",
                    width: "100%",
                    background: "#0c8dba",
                    border: "none",
                    borderRadius: "4px",
                    color: "#fff",
                  }}
                />
                <Typography
                  color="grey"
                  variant="body4"
                  style={{
                    maxWidth: "300px",
                    textAlign: "center",
                    marginTop: "1em",
                  }}
                >
                  By signing up you accept our terms and conditions & privacy
                  policy
                </Typography>
              </div>
            );
          }}
        </Formik>
      </div>
    </div>
  );
}

Register.getLayout = (page) => <RegisterLayout>{page}</RegisterLayout>;

export default Register;
