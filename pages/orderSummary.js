import React from "react";
import Image from "next/image";

import styles from "../styles/OrderSummary.module.css";
import { LayoutWithSearchBarInHeader } from "../components/Layout/PublicLayout";
import Typography from "../components/Typography/Typography";
import Counter from "../components/Counter/Counter";
import {
  ClothesCarousel,
  BooksCarousel,
} from "../components/Carousel/Carousel";
import Button from "../components/Button/Button";
import Link from "next/link";

const borderTopAndBottom = {
  borderTop: "1px solid #f6f6f6",
  borderBottom: "1px solid #f6f6f6",
  padding: "2em",
};

function OrderSummary() {
  const [orders, setOrders] = React.useState([1, 2, 3, 4]);

  const handleRemove = (i) => {
    const dte = [...orders];
    dte.splice(i, 1);
    return setOrders(dte);
  };
  return (
    <div className={styles.container}>
      <ul className={styles.crumb}>
        <li>Home</li>
        <li> > </li>
        <li>Clothing</li>
        <li> > </li>
        <li>Boy Clothings</li>
      </ul>
      <div className={styles.orderSummary}>
        <div style={{ flex: "4" }}>
          <div className={styles.leftCard}>
            <Typography
              children="Order Summary"
              variant="h3"
              color="black"
              style={{ marginBottom: "1em" }}
            />

            {orders.map((item, i) => (
              <div key={i} className={styles.item}>
                <div style={{ display: "flex" }}>
                  <Image
                    src="/assets/product4.png"
                    alt="me"
                    width="92"
                    height="79"
                  />
                  <div style={{ marginLeft: "1em" }}>
                    <Typography
                      children="Baby Trend Serene Nursery Center, Hello Kitty Classic Dot"
                      variant="h3"
                      color="black"
                      style={{ marginBottom: "1em" }}
                    />

                    <Typography
                      children="In Stock"
                      variant="body3"
                      color="black"
                      style={{ marginBottom: "1em", color: "green" }}
                    />
                    <div className={styles.counterContainer}>
                      <Typography
                        children="Quantity"
                        variant="body2"
                        color="grey"
                      />{" "}
                      <Counter count={9} />
                    </div>
                    <div
                      style={{
                        marginTop: "1em",
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <Button
                        text="Remove"
                        style={{
                          backgroundColor: "#fff",
                          marginRight: "1em",
                          color: "red",
                        }}
                        onClick={() => handleRemove(i)}
                      />
                      {/* <Typography
                        children="Remove"
                        variant="h6"
                        style={{ color: "#ba390c", marginRight: "1em" }}
                      />{" "} */}
                      |
                      <Typography
                        children="Add to Wishlist"
                        variant="h6"
                        style={{ color: "#0c8dba", marginLeft: "1em" }}
                      />
                    </div>
                  </div>
                </div>
                <Typography
                  children="N29,000.00"
                  variant="h3"
                  color="black"
                  style={{ marginBottom: "1em", color: "black" }}
                />
              </div>
            ))}
            {orders.length == 0 && (
              <Typography
                children="No Orders"
                variant="h1"
                color="black"
                style={{ marginBottom: "5em" }}
              />
            )}
          </div>
        </div>

        <div style={{ flex: "1", marginLeft: "1em" }}>
          <div className={styles.rightCard}>
            <Typography
              children="Order Summary"
              variant="h3"
              color="black"
              style={{ marginBottom: "1em" }}
            />
            <div style={borderTopAndBottom}>
              <div className={styles.orderDetail}>
                <Typography variant="body2" children="Items(2):" />
                <Typography variant="body2" children="N29,500.00" />
              </div>
              <div className={styles.orderDetail}>
                <Typography variant="body2" children="Order Discounts:" />
                <Typography
                  variant="body2"
                  children="-N1,500.00"
                  style={{ color: "red" }}
                />
              </div>
            </div>

            <div className={styles.orderDetail} style={borderTopAndBottom}>
              <Typography
                variant="body2"
                children=" Estimated Shipping:"
                style={{ margin: "2.31em 0" }}
              />

              <Typography
                variant="body2"
                children="  Add your Delivery address at checkout to see delivery charges"
                style={{ margin: "2.31em 0", maxWidth: "132px" }}
              />
            </div>

            <div className={styles.orderDetail} style={borderTopAndBottom}>
              <Typography variant="body2" children="Estimated Total:" />
              <Typography variant="body2" children="N28, 000.00" />
            </div>

            <Link href="/checkout">
              <Button
                text="Continue to Checkout"
                style={{
                  height: "48px",
                  width: "272px",
                  margin: "2.18em 0 1em",
                  backgroundColor: "#0c8dba",
                  color: "#ffffff",
                  alignSelf: "center",
                }}
              />
            </Link>

            <Button
              text="Continue Shopping"
              style={{
                height: "48px",
                width: "272px",
                marginBottom: "1em",
                backgroundColor: "#ffffff",
                color: "#0c8dba",
                alignSelf: "center",
                border: "1px solid #0c8dba",
              }}
            />
          </div>
        </div>
      </div>

      <div className={styles.carousel}>
        <Typography
          children="Customers who bought items in your cart also bought"
          variant="h3"
          color="black"
          style={{ marginBottom: "0.8125em", marginTop: "1em" }}
        />
        <ClothesCarousel />

        <Typography
          children="Recommended for you"
          variant="h3"
          color="black"
          style={{ marginBottom: "0.8125em", marginTop: "1em" }}
        />
        <BooksCarousel />
      </div>
    </div>
  );
}

OrderSummary.getLayout = (page) => (
  <LayoutWithSearchBarInHeader>{page}</LayoutWithSearchBarInHeader>
);

export default OrderSummary;
