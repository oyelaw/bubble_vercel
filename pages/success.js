import React from "react";
import Link from "next/link";

import Typography from "../components/Typography/Typography";
import Button from "../components/Button/Button";
import { LayoutWithSearchBarInHeader } from "../components/Layout/PublicLayout";

function Success() {
  return (
    <div
      style={{
        backgroundColor: "#E5E5E5",
        display: "flex",

        justifyContent: "center",
        alignContent: "center",
        padding: "4rem",
      }}
    >
      <div
        style={{
          width: "728px",
          height: "502px",
          backgroundColor: "white",
          justifyContent: "center",
          alignContent: "center",
          display: "flex",
          flexDirection: "column",
          padding: "2em",
        }}
      >
        <div
          style={{
            justifyContent: "center",
            alignContent: "center",
            display: "flex",
            marginBottom: "1.3125em",
          }}
        >
          <div
            style={{
              backgroundColor: "#0C8DBA",
              height: "76px",
              weight: "76px",
              padding: "2em",
              borderRadius: "50%",
              alignItems: "center",
              display: "flex",
            }}
          >
            <img src="/assets/Vector.png" height="28px" width="38px" />
          </div>
        </div>
        <Typography
          variant="h2"
          children="Thank you, your order has been placed."
          color="blue"
          style={{ marginBottom: "1.375em" }}
        />
        <Typography
          variant="body2"
          children="Please check your email for order confirmation and detailed delivery information. You also just got credited with 50 loyalty point for this purchase."
          color="black"
          style={{ marginBottom: "2em" }}
        />

        <Typography
          variant="subheading1"
          children="Shipping to"
          color="black"
          style={{ marginBottom: "1em" }}
        />

        <Typography
          variant="h6"
          children="Ifeanyi Umunnakwe"
          color="black"
          style={{ marginBottom: ".875em" }}
        />
        <Typography
          variant="body2"
          children="Quits Aviation Services Free Zone Murtala Muhammed International Airport 23401, Lagos"
          color="black"
        />

        <Link href="/notCompleted">
          <Button
            text="Continue Shopping"
            style={{ height: "48px", width: "100%", margin: "2em 0" }}
          />
        </Link>
      </div>
    </div>
  );
}

Success.getLayout = (page) => (
  <LayoutWithSearchBarInHeader>{page}</LayoutWithSearchBarInHeader>
);

export default Success;
