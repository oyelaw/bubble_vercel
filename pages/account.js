import React from "react";

import Typography from "../components/Typography/Typography";
import Button from "../components/Button/Button";
import AccountSidebar from "../components/Partials/AccountSidebar";
import Link from "next/link";

const containerStyle = {
  backgroundColor: "#E5E5E5",
  padding: "1em",
  display: "flex",
  gap: "1em",
  minHeight: "70vh",
};

const leftPane = {
  backgroundColor: "#fff",
  maxHeight: "310px",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const rightPane = {
  backgroundColor: "#fff",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  borderRadius: "4px",
};

const iconsText = {
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
};

const active = {
  backgroundColor: "#F1F4F4",
  display: "flex",
  alignItems: "center",
  margin: "1em 0",
  padding: ".4em",
};

const buttonStyle = {
  width: "100%",
  backgroundColor: "#fff",
  color: "#0c8dba",
  alignSelf: "center",
  borderTop: "1px solid #E0E0E0",
  padding: "1em",
  marginTop: ".5em",
};

const boxStyle = {
  background: "rgba(12, 141, 186, 0.03)",
  display: "flex",
  flexDirection: "column",
  padding: "1em",
  flex: "1",
};

const EditButtonStyle = {
  width: "100%",
  backgroundColor: "inherit",
  color: "#0c8dba",
  textAlign: "left",
  marginTop: ".5em",
};

export default function Account() {
  return (
    <div style={containerStyle}>
      <div style={{ flex: "1" }}>
        <AccountSidebar activeStyleIndex={1} />
      </div>

      <div style={{ flex: "4" }}>
        <div style={rightPane}>
          <Typography
            children="Account Overview"
            variant="h3"
            color="black"
            style={{ marginBottom: "1em" }}
          />
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridGap: "1em",
              borderTop: "1px solid #E0E0E0",
              paddingTop: "1em",
            }}
          >
            <div style={boxStyle}>
              <Typography
                children="Profile Information"
                variant="h6"
                color="black"
                style={{
                  paddingBottom: "1em",
                  marginBottom: "1em",
                  borderBottom: "1px solid #E0E0E0",
                }}
              />

              <Typography
                children="Your profile information can be accessed here, you can change your password and email address."
                variant="body4"
                color="black"
                style={{
                  marginBottom: "1em",
                }}
              />

              <Link href="/profile">
                <Button text="Edit Information" style={EditButtonStyle} />
              </Link>
            </div>

            <div style={boxStyle}>
              <Typography
                children="Children Profile"
                variant="h6"
                color="black"
                style={{
                  paddingBottom: "1em",
                  marginBottom: "1em",
                  borderBottom: "1px solid #E0E0E0",
                }}
              />

              <Typography
                children="Add your children’s profile, to get discounts that can be converted to cash."
                variant="body4"
                color="black"
                style={{
                  marginBottom: "1em",
                }}
              />

              <Link href="/child">
                <Button text="See Profile" style={EditButtonStyle} />
              </Link>
            </div>

            <div style={boxStyle}>
              <Typography
                children="Address Book"
                variant="h6"
                color="black"
                style={{
                  paddingBottom: "1em",
                  marginBottom: "1em",
                  borderBottom: "1px solid #E0E0E0",
                }}
              />

              <Typography
                children="View and add new delivery address here."
                variant="body4"
                color="black"
                style={{
                  marginBottom: "1em",
                }}
              />
              <Link href="/addressBook">
                <Button text="See address books" style={EditButtonStyle} />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
