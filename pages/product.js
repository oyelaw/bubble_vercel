import React from "react";
import Image from "next/image";
import ReactStars from "react-rating-stars-component";
import ProgressBar from "@ramonak/react-progress-bar";
import styles from "../styles/Product.module.css";
import Typography from "../components/Typography/Typography";
import Button from "../components/Button/Button";
import Counter from "../components/Counter/Counter";
import { BooksCarousel as Carousel } from "../components/Carousel/Carousel";
import { QuestionAndAnswerCard } from "../components/Card/Card";
import { LayoutWithSearchBarInHeader } from "../components/Layout/PublicLayout";

import Link from "next/link";

const DATA = [
  {
    header: "This is a sample question",
    answer: "This is the sample answer, all after is still valid",
    by: "By Instant Baby Store on February 25, 2021",
    vote: 5,
  },
  {
    header: "This is a sample question",
    answer: "This is the sample answer, all after is still valid",
    by: "By Instant Baby Store on February 25, 2021",
    vote: 2,
  },
  {
    header: "This is a sample question",
    answer: "This is the sample answer, all after is still valid",
    by: "By Instant Baby Store on February 25, 2021",
    vote: 4,
  },
  {
    header: "This is a sample question",
    answer: "This is the sample answer, all after is still valid",
    by: "By Instant Baby Store on February 25, 2021",
    vote: 7,
  },
];

function Ratings({ star, completed, percentage }) {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        margin: "1em 0",
      }}
    >
      <Typography
        style={{
          padding: 0,
          margin: 0,
          marginRight: "1em",
        }}
      >
        {star} star
      </Typography>
      <ProgressBar
        completed={completed}
        borderRadius="1px"
        className={styles.max}
        bgColor="#0C8DBA"
        isLabelVisible={false}
      />
      <Typography
        style={{
          padding: 0,
          margin: 0,
          marginLeft: ".4em",
        }}
      >
        {percentage}%
      </Typography>
    </div>
  );
}

function Product() {
  return (
    <div className={styles.container}>
      <ul className={styles.crumb}>
        <li>Home</li>
        <li> > </li>
        <li>Clothing</li>
        <li> > </li>
        <li>Boy Clothings</li>
      </ul>
      <div className={styles.main}>
        <div className={styles.pane}>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {/* start 1 */}
            <div className={styles.smallImageWrapper}>
              <Image
                src="/assets/smallProduct.png"
                alt="me"
                width="72"
                height="61"
              />
            </div>
            {/* end 1 */}
            {/* start 2 */}
            <div className={styles.smallImageWrapper}>
              <Image
                src="/assets/smallProduct.png"
                alt="me"
                width="72"
                height="61"
              />
            </div>
            {/* end */}
            {/* start 3 */}
            <div className={styles.smallImageWrapper}>
              <Image
                src="/assets/smallProduct.png"
                alt="me"
                width="72"
                height="61"
              />
            </div>
            {/* end 3 */}
          </div>

          <div>
            <Image
              src="/assets/bigProduct.png"
              alt="me"
              width="446"
              height="377"
            />
          </div>
          <div className={styles.details}>
            <div className={styles.titleRating}>
              <Typography
                variant="h3"
                children="Baby Trend Serene Nursery Center, Hello Kitty Classic Dot"
                color="black"
                style={{ marginBottom: "1em" }}
              />

              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  borderBottom: "1px solid grey",
                  padding: "1em 1em 1em 0",
                }}
              >
                <ReactStars
                  count={5}
                  size={30}
                  edit={false}
                  value={4}
                  activeColor="#ffd700"
                />

                <Typography
                  children="(32 reviews) | 5 Questions Answered"
                  variant="body4"
                  color="blue"
                  style={{ marginLeft: ".3em" }}
                />
              </div>

              <Typography
                children="Price"
                variant="body4"
                color="black"
                style={{ marginTop: "2em", marginBottom: ".4em" }}
              />

              <Typography
                children="N13,000"
                variant="h2"
                color="black"
                style={{ marginBottom: "1.25em" }}
              />
            </div>
            <div className={styles.colors}>
              <Typography
                children="Colors"
                variant="body2"
                color="black"
                style={{ minWidth: "45px" }}
              />
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                <div
                  className={styles.thumbnailWrapper}
                  style={{ border: "1px solid #0C8DBA" }}
                >
                  <Image
                    src="/assets/smallProduct.png"
                    alt="me"
                    width="40"
                    height="40"
                  />
                </div>

                <div className={styles.thumbnailWrapper}>
                  <Image
                    src="/assets/smallProduct.png"
                    alt="me"
                    width="40"
                    height="40"
                  />
                </div>

                <div className={styles.thumbnailWrapper}>
                  <Image
                    src="/assets/smallProduct.png"
                    alt="me"
                    width="40"
                    height="40"
                  />
                </div>
                <div className={styles.thumbnailWrapper}>
                  <Image
                    src="/assets/smallProduct.png"
                    alt="me"
                    width="40"
                    height="40"
                  />
                </div>
              </div>
            </div>

            <div className={styles.colors}>
              <Typography
                children="Size"
                variant="body2"
                color="black"
                style={{ minWidth: "45px" }}
              />
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                }}
              >
                <div
                  className={styles.sizeWrapper}
                  style={{ backgroundColor: "#4A4A4A", color: "#fff" }}
                >
                  10
                </div>

                <div className={styles.sizeWrapper}>11</div>

                <div className={styles.sizeWrapper}>12</div>
              </div>
            </div>
            <ul className={styles.points}>
              <li>
                <Typography
                  children=" Solid colors: 100% Cotton; Heather Grey: 90% Cotton, 10%
                Polyester"
                  variant="body3"
                  color="black"
                />
              </li>
              <li>
                <Typography children="Imported" variant="body3" color="black" />
              </li>
              <li>
                <Typography
                  children="Machine wash cold with like colors, dry low heat"
                  variant="body3"
                  color="black"
                />
              </li>
              <li>
                <Typography
                  children="Officially Licensed Star Wars Tee Shirt"
                  variant="body3"
                  color="black"
                />
              </li>
              <li>
                <Typography
                  children="Lightweight, Classic fit, Double-needle sleeve and bottom hem"
                  variant="body3"
                  color="black"
                />
              </li>
            </ul>
          </div>
          <div className={styles.checkoutBox}>
            <Typography children="N13, 000" variant="h3" color="black" />
            <Typography
              children="In Stock"
              variant="body2"
              color="black"
              style={{ color: "green" }}
            />

            <div className={styles.counterContainer}>
              <Typography children="Quantity" variant="body2" color="grey" />
              <Counter count={2} />
            </div>

            <Button
              text="ADD TO CART"
              style={{
                width: "100%",
                padding: "0.6em",
                backgroundColor: "#ffffff",
                color: "#0c8dba",
                border: "1px solid #0c8dba",
                marginTop: "1em",
              }}
            />

            <Link href="/orderSummary">
              <Button
                text="BUY NOW"
                style={{
                  width: "100%",
                  padding: "0.6em",
                  backgroundColor: "#0c8dba",
                  color: "#ffffff",
                  border: "1px solid #0c8dba",
                  marginTop: "1em",
                }}
              />
            </Link>

            <Button
              text="ADD TO WISHLIST"
              style={{
                width: "100%",
                padding: "1em",
                backgroundColor: "#ffffff",
                color: "#03232e",
                border: "none",
                borderTop: "1px solid #E0E0E0",
                marginTop: "1em",
              }}
            />
          </div>
        </div>
      </div>

      <div className={styles.productDescription}>
        <Typography
          children="Product description"
          variant="h3"
          color="black"
          style={{ marginBottom: "0.4em" }}
        />
        <Typography
          children="Polyester Heatgear technology, crew neck, short sleeve. Loose fit"
          variant="body3"
          color="black"
          style={{
            marginBottom: "0.4em",
            padding: "1em 0",
            borderTop: "1px solid #e0e0e0",
          }}
        />
      </div>

      <div className={styles.productDescription}>
        <Typography
          children="Product Details"
          variant="h3"
          color="black"
          style={{ marginBottom: "0.4em" }}
        />

        <div
          style={{
            display: "flex",
            alignItems: "center",
            borderTop: "1px solid #e0e0e0",
          }}
        >
          <Typography
            children="Package Dimensions:"
            variant="h6"
            color="black"
            style={{
              padding: ".6em 0",
            }}
          />
          <Typography
            children="8.9 x 8.11 x 2.01 inches; 1 Pounds"
            variant="body3"
            color="black"
            style={{
              padding: ".6em 0",
              marginLeft: ".5em",
            }}
          />
        </div>

        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Typography
            children="Department:"
            variant="h6"
            color="black"
            style={{
              padding: ".6em 0",
            }}
          />
          <Typography
            children="Mens"
            variant="body3"
            color="black"
            style={{
              padding: ".6em 0",
              marginLeft: ".5em",
            }}
          />
        </div>

        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Typography
            children="Date First Available:"
            variant="h6"
            color="black"
            style={{
              padding: ".6em 0",
            }}
          />
          <Typography
            children="February 16, 2021"
            variant="body3"
            color="black"
            style={{
              padding: ".6em 0",
              marginLeft: ".5em",
            }}
          />
        </div>
      </div>
      <div className={styles.productDescription}>
        <Typography
          children="Question & Answer"
          variant="h3"
          color="black"
          style={{ marginBottom: "0.4em" }}
        />
        <div className={styles.questions}>
          <div className={styles.results}>
            <input
              className={styles.input}
              type="text"
              placeholder="Have a question? Search for answer"
            />
            <Button
              style={{
                height: "48px",
                backgroundColor: "#f0f1f1",
                border: "none",
                color: "#03232E",
                width: "206px",
              }}
              text="Post a question"
            />
          </div>

          <div className={styles.results}>
            {DATA.map((item, i) => {
              const { vote, header, answer, by } = item;
              return (
                <QuestionAndAnswerCard
                  vote={vote}
                  header={header}
                  answer={answer}
                  postedBy={by}
                />
              );
            })}
          </div>
        </div>
      </div>

      <div className={styles.productDescription}>
        <Typography
          children="Customer Review"
          variant="h3"
          color="black"
          style={{ marginBottom: "0.4em" }}
        />
        <div className={styles.customerReview}>
          <div>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                padding: "1em",
              }}
            >
              <ReactStars count={5} size={15} activeColor="#ffd700" value={4} />
              <Typography
                style={{
                  padding: 0,
                  margin: 0,
                  marginLeft: "0.3em",
                }}
              >
                4.5 out of 5
              </Typography>
            </div>
            <Ratings star={5} completed={60} percentage={60} />
            <Ratings star={4} completed={10} percentage={10} />
            <Ratings star={3} completed={18} percentage={18} />
            <Ratings star={2} completed={12} percentage={12} />
            <Ratings star={1} completed={8} percentage={8} />

            <Typography
              children="Share your thoughts with other customers"
              variant="h6"
              color="black"
              style={{ marginTop: "3.5em", marginBottom: "2em" }}
            />
            <Button
              text="Write a review"
              style={{
                backgroundColor: "#f0f1f1",
                border: "none",
                width: "348px",
                height: "48px",
                color: "#03232e",
              }}
            />
          </div>

          <div className={styles.reviews}>
            {[1, 2, 3, 4, 5, 6, 7].map((item, i) => {
              return (
                <div className={styles.review}>
                  <Typography
                    children="My favorite shirt"
                    variant="h3"
                    color="black"
                  />
                  <ReactStars
                    count={5}
                    value={3}
                    size={10}
                    activeColor="#ffd700"
                  />
                  <Typography
                    children="By Austine on February 25, 2021"
                    variant="body4"
                    color="grey"
                  />
                  <Typography
                    children="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tincidunt
      malesuada montes, tortor tellus faucibus senectus ultrices."
                    variant="body4"
                    color="grey"
                    style={{ margin: "1em 0" }}
                  />
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                    }}
                  >
                    <Typography
                      children="66 people found this helpful. Was this helpful to you?"
                      variant="body4"
                      color="black"
                      style={{ marginRight: ".1em" }}
                    />
                    <div style={{ display: "flex" }}>
                      <Button
                        text="Yes"
                        style={{
                          backgroundColor: "#f0f1f1",
                          width: "39px",
                          height: "26px",
                          marginRight: "1em",
                          color: "black",
                        }}
                      />
                      <Button
                        text="No"
                        style={{
                          backgroundColor: "#f0f1f1",
                          width: "39px",
                          height: "26px",
                          color: "black",
                        }}
                      />
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>

      <div className={styles.carousel}>
        <Typography
          children="Top Selling Products"
          variant="h3"
          color="black"
          style={{ marginBottom: "0.8125em", maxWidth: "195px" }}
        />
        <Carousel />
      </div>
    </div>
  );
}

Product.getLayout = (page) => (
  <LayoutWithSearchBarInHeader>{page}</LayoutWithSearchBarInHeader>
);

export default Product;
