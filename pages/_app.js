import React from "react";
import App from "next/app";

import "@fortawesome/fontawesome-svg-core/styles.css";
import { config } from "@fortawesome/fontawesome-svg-core";
config.autoAddCss = false;

// import { library } from "@fortawesome/fontawesome-svg-core";
// import { fab } from "@fortawesome/free-brands-svg-icons";
// import {
//   faCoffee,
//   faFont,
//   faAngleLeft,
// } from "@fortawesome/free-solid-svg-icons";

import Layout from "../components/Layout/Layout";

import "../styles/globals.css";

// library.add(fab, faCoffee, faAngleLeft);

class MyApp extends App {
  render() {
    const { Component, pageProps, router } = this.props;

    const getLayout =
      Component.getLayout || ((page) => <Layout>{page}</Layout>);

    return getLayout(<Component {...pageProps} />);
  }
}

export default MyApp;
